#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Aplicações distribuídas - Projeto 2 - lock_skel.py
Grupo: 20
Alunos: 43551 45802 43304
"""
# Zona para fazer imports
import thread

import lock_pool as l
import pickle as p
import time as t

class LockSkel:

    def __init__(self,rs,K,time):
        self.rs = rs    
        self.lp = l.lock_pool(rs,K,time)
        thread.start_new_thread(self.expireChecker, ())

    def expireChecker(self):
        while (True):
            print "Checking locks"
            self.lp.clear_expired_locks()
            t.sleep(1)

    def handle(self, cms):

        if cms[0] == '10':
            msg = self.lock(cms)
        elif cms[0] == '20':
            msg = self.release(cms)
        elif cms[0] == '30':
            msg = self.test(cms)
        elif cms[0] == '40':
            msg = self.stats(cms)
        elif cms[0] == '50':
            msg = self.statsK(cms)
        else:
            print "Comando Errado"
            msg = "cant do op"

        return p.dumps(msg, -1)

    def lock(self, cms):

        msg = list()
        msg.append('11')

        try:
            if cms[2] > self.rs:
                msg.append('None')
            elif self.lp.lock(cms[2], cms[1], t.time()):
                msg.append('True')
            else:
                msg.append('False')

        except IndexError:
            msg.append('NOK')
            print 'skel - IndexError'
        except UnboundLocalError:
            msg.append('NOK')
            print 'skel - UnboundLocalError'
        return msg

    def release(self, cms):

        msg = list()
        msg.append('21')

        try:
            if cms[2] > self.rs:
                msg.append('None')
            elif self.lp.release(cms[2], cms[1]):
                msg.append('True')
            else:
                msg.append('False')

        except IndexError:
            msg.append('NOK')
            print 'skel - IndexError'
        except UnboundLocalError:
            msg.append('NOK')
            print 'skel - UnboundLocalError'
        return msg

    def test(self, cms):

        msg = list()
        msg.append('31')

        try:
            if cms[1] > self.rs:
                msg.append('None')

            elif self.lp.test(cms[1]):
                msg.append('True')
            else:
                msg.append('False')

        except IndexError:
            msg.append('NOK')
            print 'skel - IndexError'
        except UnboundLocalError:
            msg.append('NOK')
            print 'skel - UnboundLocalError'

        return msg

    def stats(self, cms):

        msg = list()
        msg.append('41')

        try:
            if cms[1] > self.rs:
                msg.append('None')
            else:
                msg.append(self.lp.stat(cms[1]))

        except IndexError:
            msg.append('NOK')
            print 'skel - IndexError'
        except UnboundLocalError:
            msg.append('NOK')
            print 'skel - UnboundLocalError'

        return msg

    def statsK(self, cms):
        msg = list()
        msg.append('51')

        try:
            if cms[1] > self.rs:
                msg.append('None')
            else:
                msg.append(self.lp.stat_k(cms[1]))

        except IndexError:
            msg.append('NOK')
            print 'skel - IndexError'
        except UnboundLocalError:
            msg.append('NOK')
            print 'skel - UnboundLocalError'

        return msg
