"""
Aplicações distribuídas - Projeto 2 - lock_client.py
Grupo: 20
Alunos: 43551 45802 43304
"""

Quando a aplicação cliente é iniciada, executando o lock_client.py (informações mais à frente), e a ligação com o servidor é estabelecida, esta manda uma mensagem com o Id do client. Esta mensagem segue o mesmo padrão das outras, usando o código 00, assim a mensagem enviada pelo cliente fica: [00, <id do cliente>].
A aplicação do servidor, inicidada executando o lock_server.py (informações mais à frente), recebe esta mensagem e verifica se o id já se encontra em uso. Se sim, responde com [01, 'NOK'], se não, responde com [01, 'OK'].
A aplicação cliente recebe a resposta e verifica se tem permissão ou não. No caso de receber a resposta 'OK', informa o utilizador que a ligação foi estabelecida, no caso de 'NOK' informa, de forma legivél e facilmente compreencivel, que o ID usado já está em uso.

lock_client.py:
    É o ficheiro python que contem o programa executável do cliente, este importa o ficheiro lock_stub.py (que contem a class LockStub), que por sua vez importa o net_client.py (que contem a class NetClient) que por sua vez importa o sock_utils.py
    A aplicação tem um handler para lidar com as chamadas ctrl+z e ctrl+c 
    O client deve ser chamado da seguinte maneira:
        $ ​python lock_client.py IP porto id

lock_server.py:
    É o ficheiro python que contem o programa executável do servidor, este importa os ficheiros sock_utils.py e lock_skel.py (que contem a class LockSkel), o lock_skel.py, por sua vez importa o lock_pool.py a class resource_lock e a class lock_pool.py (que contem ambas as classes lock_pool e resource_lock)
    A aplicação tem um handler para lidar com as chamadas ctrl+z e ctrl+c 
    O servidor deve ser chamado das seguintes maneiras:
      · python lock_server.py porto num_de_recursos max_boqueios_simultaneos​ tempo_de_concessão
      · python lock_server.py
    Se o servidor for corrido sem argumentos, ou com argumentos inválidos, utilizará os seguintes valores padrão:
        porto​ = 9999
        num_de_recursos = 10
        max_boqueios_simultaneos​ = 5
        tempo_de_concessão = 10
