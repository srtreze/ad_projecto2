#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Aplicações distribuídas - Projeto 2 - lock_pool.py
Grupo: 20
Alunos: 43551 45802 43304
"""
# Zona para fazer imports

import time as t

class resource_lock:
    def __init__(self, K):
        """
        Define e inicializa as características de um LOCK num recurso.
        """
        self.lock_state = False
        self.lock_counter = 0
        self.resource_owner = 0
        self.locktime_by_owner = []
        self.time_expire = 0
        self.max_clients = K

    def lock(self, client_id, time_limit):
        """
        Bloqueia o recurso se este não estiver bloqueado ou mantém o bloqueio
        se o recurso estiver bloqueado pelo cliente client_id. Neste caso renova
        o bloqueio do recurso até time_limit.
        Retorna True se bloqueou o recurso ou False caso contrário.
        """

        if (not (client_id in self.locktime_by_owner)) and self.max_clients > len(self.locktime_by_owner):
            if len(self.locktime_by_owner) == 0 and self.resource_owner == 0:
                self.lock_counter += 1
                self.lock_state = True
                self.time_expire = time_limit
                self.resource_owner=client_id
            elif self.resource_owner == 0:
                self.lock_state=True
                self.lock_counter += 1
                self.time_expire = time_limit
                self.locktime_by_owner.append(client_id)
            else:
                self.locktime_by_owner.append(client_id)
            return True
        elif client_id == self.resource_owner:
            self.lock_counter += 1
            return True

        return False



    def urelease(self):
        """
        Liberta o recurso incondicionalmente, alterando os valores associados
        ao bloqueio.
        """
        self.lock_state = False
        self.time_expire = 0
        self.resource_owner=0

    def release(self, client_id):
        """
        Liberta o recurso se este foi bloqueado pelo cliente client_id,
        retornando True nesse caso. Caso contrário retorna False.
        """
        if client_id == self.resource_owner:
            if len(self.locktime_by_owner) == 0:
                self.resource_owner = 0
                self.lock_state = False
            else:
                self.resource_owner = self.locktime_by_owner[0]
                self.locktime_by_owner.pop(self.resource_owner)
            return True
        elif client_id in self.locktime_by_owner:
            self.locktime_by_owner.pop(client_id)
            return True
        return False

    def test(self):
        """
        Retorna o estado de bloqueio do recurso.
        """
        return self.lock_state

    def stat(self):
        """
        Retorna o número de vezes que este recurso já foi bloqueado.
        """
        return self.lock_counter

    def stat_k(self):
        """
        Retorna o número de bloqueios em simultâneo em K.
        """
        if len(self.locktime_by_owner)==0 and self.resource_owner!=0:
            return 1
        else:
            return len(self.locktime_by_owner)

    def time(self):
        return self.time_expire


###############################################################################

class lock_pool:
    def __init__(self, N, K, time):
        """
        Define um array com um conjunto de locks para N recursos. Os locks podem
        ser manipulados pelos métodos desta classe.
        Define também K, o valor máximo do número de bloqueios simultâneos a um recurso qualquer.
        """
        self.time = time
        self.lock_pool_array = []
        for i in range(N):
            self.lock_pool_array.append(resource_lock(K))

    def clear_expired_locks(self):
        """
        Verifica se os recursos que estão bloqueados ainda estão dentro do tempo
        de concessão do bloqueio. Liberta os recursos caso o seu tempo de
        concessão tenha expirado.
        """

        for resource_lock in self.lock_pool_array:
            if resource_lock.time_expire < t.time() and resource_lock.time_expire != 0:
                resource_lock.urelease()
                print "Expirou o actual"
            if len(resource_lock.locktime_by_owner) > 0 and not resource_lock.lock_state:
                print "mudei para o cliente: " + str(resource_lock.locktime_by_owner[0])
                newOwner=resource_lock.locktime_by_owner[0]
                resource_lock.locktime_by_owner.pop(0)
                resource_lock.lock(newOwner, t.time() + self.time)
                resource_lock.resource_owner = newOwner



    def lock(self, resource_id, client_id, time_limit):
        """
        Tenta bloquear o recurso resource_id pelo cliente client_id, até ao
        instante time_limit.
        O bloqueio do recurso só é possível se K ainda não foi excedido para este
        recurso. É aconselhável implementar um método _try_lock para vericar estas condições.
        Retorna True em caso de sucesso e False caso contrário.
        """
        return self.lock_pool_array[resource_id-1].lock(client_id, time_limit+self.time)

    def release(self, resource_id, client_id):
        """
        Liberta o bloqueio sobre o recurso resource_id pelo cliente client_id.
        True em caso de sucesso e False caso contrário.
        """
        return self.lock_pool_array[resource_id-1].release(client_id)

    def test(self, resource_id):
        """
        Retorna True se o recurso resource_id estiver bloqueado e False caso
        contrário.
        """
        return self.lock_pool_array[resource_id].test()

    def stat(self, resource_id):
        """
        Retorna o número de vezes que o recurso resource_id já foi bloqueado.
        """
        return self.lock_pool_array[resource_id].stat()

    def stat_k(self, resource_id):
        """
        Retorna o número de bloqueios simultâneos no recurso resource_id.
        """
        return self.lock_pool_array[resource_id].stat_k()

    def __repr__(self):
        """
        Representação da classe para a saída standard. A string devolvida por
        esta função é usada, por exemplo, se uma instância da classe for
        passada à função print.
        """
        output = ""
        counter = 0
        for lock in self.lock_pool_array:
            output += "recurso " + str(counter) + " bloqueado pelo cliente " + str(lock.resource_owner) + "\n"
            counter += 1
        return output