#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Aplicações distribuídas - Projeto 2 - lock_server.py
Grupo: 20
Alunos: 43551 45802 43304
"""
# Zona para fazer imports

import sys
import signal
import sock_utils
import pickle as p
import select as sel
import lock_skel as skel


def handler(signum, frame):
    print ""
    print 'closing socket and program...'
    sock.close()
    sys.exit()

# Control + z and Control + c handlers
signal.signal(signal.SIGTSTP, handler)
signal.signal(signal.SIGINT, handler)

if len(sys.argv) > 3:
    try:
        HOST = ''
        PORT = int(sys.argv[1])
        resource_number = int(sys.argv[2])
        max_client=int(sys.argv[3])
        resource_time = int(sys.argv[4])
    except:
        print "Input incorrecto, a usar os parametros padrão"
        HOST = ''
        PORT = 9999
        resource_number = 10
        max_client = 10
        resource_time = 10
else:
    HOST = ''
    PORT = 9999
    resource_number = 10
    max_client = 10
    resource_time = 10
    print "A utiizar os parametros padrão"

print "Porta: " + str(PORT)
print "Recursos: " + str(resource_number) + " Tempo: " + str(resource_time)



# iniciar o skel
lskel = skel.LockSkel(resource_number,max_client,resource_time)

# iniciar sock e arrays extra
# msgcliente = []
# ret = []
sock = sock_utils.create_tcp_server_socket(HOST, PORT, 5)

# variaveis para select
client_ids = []
SocketInput = [sock]
SocketOutput = []

while True:
    try:
        R, W, X = sel.select(SocketInput, SocketOutput, [])
        for soquete in R:
            try:
                if soquete is sock:
                    (conn_sock, addr) = sock.accept()
                    recvv = conn_sock.recv(50)
                    tamanho = int(p.loads(recvv))
                    conn_sock.sendall(p.dumps("SIZEOK", -1))
                    comando = sock_utils.receive_all(conn_sock, tamanho)
                    if tamanho == sys.getsizeof(comando):
                        msg_unp = p.loads(comando)
                        if msg_unp[0] == '00':
                            id = int(msg_unp[1])
                            flag = False
                            if id in client_ids:
                                flag = True
                            if not flag:
                                SocketInput.append(conn_sock)
                                client_ids.append(id)
                                print "Cliente" + str(conn_sock.getpeername()) + "\nID " + str(id)
                                msg = list()
                                msg.append('01')
                                msg.append('OK')
                                res = p.dumps(msg, -1)
                                conn_sock.sendall(res)
                            else:
                                msg = list()
                                msg.append('01')
                                msg.append('NOK')
                                res = p.dumps(msg, -1)
                                conn_sock.sendall(res)
                else:
                    resp = []
                    soqueteID = client_ids[R.index(soquete)]
                    recvv = soquete.recv(50)
                    if recvv == '':
                        print 'Client Close' + str(soquete.getpeername())
                        soquete.close()
                        SocketInput.remove(soquete)
                        client_ids.remove(soqueteID)
                    else:
                        tamanho = int(p.loads(recvv))
                        soquete.sendall(p.dumps("SIZEOK", -1))
                        comando = sock_utils.receive_all(soquete, tamanho)
                        if tamanho == sys.getsizeof(comando):
                            try:
                                msg_unp = p.loads(comando)
                                print 'Pedido Recebido: ' + str(msg_unp)
                                print ""
                                msg_unp[1] = int(msg_unp[1])
                                if len(msg_unp) > 2:
                                    if msg_unp[1] == soqueteID:
                                        msg_unp[2] = int(msg_unp[2])
                                        resp = lskel.handle(msg_unp)
                                        soquete.sendall(resp)
                                    else:
                                        msg = list()
                                        msg.append("ID doesn't correspond")
                                        soquete.sendall(msg)
                                else:
                                    resp = lskel.handle(msg_unp)
                                    soquete.sendall(resp)
                            except IndexError:
                                msg = list()
                                msg.append('The resource requested does not exist')
                                soquete.sendall(msg)

                            except ValueError:
                                msg = list()
                                msg.append('The message sent was not valid')
                                soquete.sendall(msg)

                        else:
                            print 'Erro: Mensagem recebida nao tem o mesmo tamanho da original'
                            soquete.close()
                            SocketInput.remove(soquete)
            except IOError:
                soquete.close()
                SocketInput.remove(soquete)
                print "Unexpected error:", sys.exc_info()[0]
    except KeyboardInterrupt:
        print 'closing socket and program...'
        sock.close()
        sys.exit()
sock.close()
