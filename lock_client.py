#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Aplicações distribuídas - Projeto 2 - lock_client.py
Grupo: 20
Alunos: 43551 45802 43304
"""
# Zona para fazer imports

import sys
import signal
import lock_stub as ls
import socket


def handler(signum, frame):
    print ""
    print 'closing socket and program...'
    lstub.close()
    sys.exit()

# Control + z and Control + c handlers
signal.signal(signal.SIGTSTP, handler)
signal.signal(signal.SIGINT, handler)

# Programa principal

client_commands = ["TEST", "STATS", "STATS_K","EXIT"] #"LOCK", "RELEASE",
client_id_commands = ["LOCK", "RELEASE"]


if len(sys.argv) > 3:
    try:
        HOST = sys.argv[1]
        PORT = int(sys.argv[2])
        ID = int(sys.argv[3])
    except TypeError:
        print "Parametros Incorretos"
        sys.exit()

    flag = False
    try:
        lstub = ls.LockStub(HOST, PORT)
        res = lstub.send_id(ID)
        if res[1] == 'OK':
            print "Connected with ID " + str(ID)
            flag = True
        else:
            print "ID " + str(ID) + " already in use"
    except socket.error:
        print "Problema ao tentar ligar com o servidor"
        print 'closing program... Retry later'
        sys.exit()

    if flag:
        try:
            while True:
                msg = raw_input("Comando: ")
                msg = msg.split(" ")

                try:
                    for index, arg in enumerate(msg):
                        if msg[index] not in client_commands and msg[index] not in client_id_commands:
                            msg[index] = int(msg[index])
                except:
                    print "String nos argumentos sem ser no comando!"
                    continue

                # verificacao do comando

                if msg[0] == "EXIT":
                    sys.exit()

                if len(msg) <= 1 or (msg[0] not in client_commands and msg[0] not in client_id_commands):
                    print "verificar comando"
                    continue

                if msg[0] in client_id_commands:
                    msg.insert(1, ID)

                if (msg[0] in client_commands and len(msg) == 2) or (msg[0] in client_id_commands and len(msg) == 3):

                    resposta = ''

                    if msg[0] == 'LOCK':
                        resposta = lstub.lock(msg)

                    elif msg[0] == 'RELEASE':
                        resposta = lstub.release(msg)

                    elif msg[0] == 'TEST':
                        resposta = lstub.test(msg)

                    elif msg[0] == 'STATS':
                        resposta = lstub.stats(msg)

                    elif msg[0] == 'STATS_K':
                        resposta = lstub.statsK(msg)

                    print 'Pedido Recebido: %s' % str(resposta)
                    print ""

                else:
                    print "O comando foi mal feito."
                    continue
        except KeyboardInterrupt:
            print ""
            print 'closing socket and program...'
            lstub.close()
            sys.exit()

        except socket.error:
            print "\nProblema ao receber resposta do servidor"
            print 'closing socket and program... Retry later'
            lstub.close()
            sys.exit()
    else:
        print 'Retry with different id'
        lstub.close()
        sys.exit()

else:
    print "Sem argumentos ou argumentos incompletos"
